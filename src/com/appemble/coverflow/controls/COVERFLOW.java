/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.coverflow.controls;

import java.util.ArrayList;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Cache;
import com.appemble.avm.ConfigManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.controls.ControlInterface;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.coverflow.helper.CoverflowArrayAdaptor;
import com.appemble.coverflow.helper.CoverflowCursorAdaptor;
import com.appemble.coverflow.helper.CoverflowJsonAdaptor;

public class COVERFLOW extends Gallery implements ControlInterface {
	ControlModel controlModel;
	ScreenModel screenModel;
    Vector<ControlModel> vCoverflowControls;
	public boolean bOnMeasure = false;
	public static final int ON_CREATE_COVERFLOW_IMAGE = "ON_CREATE_COVERFLOW_IMAGE".hashCode();
	public static final int ON_EMPTY_COVERFLOW = "ON_EMPTY_COVERFLOW".hashCode();
	float fWidth, fHeight; // Using which the child control dimensions are to be calculated.
	
    /**
     * Graphics Camera used for transforming the matrix of ImageViews.
     */
    private final Camera mCamera = new Camera();

    /**
     * The maximum angle the Child ImageView will be rotated by.
     */
    private int mMaxRotationAngle = 60;

    /**
     * The maximum zoom on the centre Child.
     */
    private int mMaxZoom = -120;

    /**
     * The Centre of the Coverflow.
     */
    private int mCoveflowCenter;

	public COVERFLOW(final Context context, ControlModel controlObject) {
		super(context);
		controlModel = controlObject;		
        this.setStaticTransformationsEnabled(true);
	}

	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel scrModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);

		screenModel = scrModel;
		this.vCoverflowControls = vChildControls;
		if (vCoverflowControls == null || vCoverflowControls.size() == 0 || 
           vCoverflowControls.get(0).iType != ConfigManager.getInstance().getControlId(Constants.IMAGE)) {
  		    LogManager.logError("A Coverflow control must contain an IMAGE as a child control: " +
  		        controlModel.sName);
  		    return Boolean.valueOf(false);
		}
		if (Cache.bDimenRelativeToParent) {
			PointF size = DynamicLayout.calculateSize(controlModel, fParentWidth, fParentHeight);
			fWidth = size.x; fHeight = size.y;
		} else {
			fWidth = screenModel.fWidthInPixels; fHeight = screenModel.fHeightInPixels;
		}
		parseAttributes();
        if (this instanceof COVERFLOW)
            this.setOnItemClickListener(new OnItemClickListener() {
                public void onItemClick(AdapterView<?> coverflowView, View coverflowItemView, 
                    int position, long id) {
                      Action.callAction(getContext(), coverflowView, coverflowItemView, controlModel, 
                          Constants.TAP); 
			}
        });
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null, 
				controlModel.sBackgroundImage);
		return Boolean.valueOf(true);
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		bOnMeasure = true;
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		bOnMeasure = false;
		return;
	}
	
    public void onDestroy(Bundle targetParameterValueList) {
        // No need to close the cursor. Android is doing OK in closing the cursor.
		SpinnerAdapter spinnerAdapter = getAdapter();
		if (null == spinnerAdapter)
			return;
		if (spinnerAdapter instanceof CoverflowCursorAdaptor) {
			CoverflowCursorAdaptor adaptor = (CoverflowCursorAdaptor)spinnerAdapter;
			Cursor c = adaptor.getCursor();
			if (c != null && false == c.isClosed())
				c.close();
		}
	}

	public int getControlId() {
		return controlModel.id;
	}

	public ControlModel getControlModel() {
		return controlModel;
	}

	public Object getValue() {
	  // TODO return the image source present at the center.
		return null;
	}
	
	public void setValue(Object object) {
		// If called from DynamicActivity::UpdateControl, then
		// this value is obtained in the following order remote data source, local data source, remote data, 
		// local cursor, target parameter list and default value.
		if (bOnMeasure)
			return;
		try {
			if (controlModel.iDataSourceType == Constants.JSONARRAY) {
				JSONArray jsonArray = null;
				if (null != object)
					jsonArray = new JSONArray((String)object);
				setJson(jsonArray);
			}
		    else if (controlModel.iDataSourceType == Constants.ARRAYLIST)
	        	setArrayList(object);
			else if (controlModel.iDataSourceType == Constants.CURSOR)
				setCursor((Cursor) object);
			else 
			  LogManager.logError("No data_source_type for control: " + controlModel.sName);			
		} catch (JSONException jsone) {
			LogManager.logError("The control: " + controlModel.sName + " expects data type as JSONArray"); 
		} catch (ClassCastException cce) {
			LogManager.logError("The control: " + controlModel.sName + " expects data type as " + ConfigManager.getInstance().getDataTypesString(controlModel.iDataType));
		}
	}
	
	public boolean setCursor(Cursor c) {
		// The cursor can be null. Which means that the data is being removed from the list box
		SpinnerAdapter adaptor = (SpinnerAdapter)getAdapter();
		if (null == adaptor) { // the constructor does not set the adaptor.
			adaptor = new CoverflowCursorAdaptor(getContext(), this, c, controlModel, 
			    vCoverflowControls, fWidth, fHeight, screenModel);
	        setAdapter(adaptor); 
		} else {
			((CursorAdapter)adaptor).changeCursor(c);
			((CursorAdapter)adaptor).notifyDataSetChanged();
		}
		return showEmptyCoverflowControls(null == c || c.getCount() == 0);
	}

	public boolean setArrayList(Object object) {
		// here parse the string to formulate the multidimensional array. The order of the controls in the list box 
		// determine the order in which they get the value from this multidimensional array.
		
		// String [] aValues = sValues.split(",");
	    if (false == (object instanceof ArrayList<?>))
	        return false;
		ArrayList<String> aValues = new ArrayList<String>();
		// to represent multidimensional array, use HashMap<columm name, value> in an ArrayList
		// ArrayList<HashMap<String, String>> aValues = new ArrayList<HashMap<String, String>>();
		if (null == aValues || aValues.size()== 0)
			return false;
		SpinnerAdapter adaptor = getAdapter();
		if (null == adaptor) { // the constructor does not set the adaptor.
			adaptor = new CoverflowArrayAdaptor(getContext(), this, aValues, controlModel, 
			    vCoverflowControls, fWidth, fHeight, screenModel);
	        setAdapter(adaptor); 
		} else {
			((CoverflowArrayAdaptor)adaptor).changeDataset(aValues);
			((CoverflowArrayAdaptor)adaptor).notifyDataSetChanged();
		}
		return showEmptyCoverflowControls(null == aValues || aValues.size() == 0);
	}
	
	public boolean setJson(Object object) {		
		JSONArray jsonArray = (JSONArray) object;
		SpinnerAdapter adaptor = getAdapter();
		if (null == adaptor) { // the constructor does not set the adaptor.
			adaptor = new CoverflowJsonAdaptor(getContext(), this, jsonArray, controlModel, 
			    vCoverflowControls, fWidth, fHeight, screenModel);
	        setAdapter(adaptor); 
		} else {
			((CoverflowJsonAdaptor)adaptor).changeDataset(jsonArray);
			((CoverflowJsonAdaptor)adaptor).notifyDataSetChanged();
//			final ListJsonAdaptor a = adaptor;
//			AppembleActivity appembleActivity = Utilities.getActivity(getContext(), this, null);
//			if (appembleActivity instanceof DynamicActivity) {
//				DynamicActivity dynamicActivity = (DynamicActivity) appembleActivity;
//				dynamicActivity.runOnUiThread(new Runnable() {
//				    public void run() {
//				        a.notifyDataSetChanged();
//				    }
//				});			
//			}
		}
		return showEmptyCoverflowControls(null == jsonArray || jsonArray.length() == 0);
	}

	private boolean showEmptyCoverflowControls(boolean bVisibility) {
		String sEmptyListControl = controlModel.mExtendedProperties.get("on_empty_show");
		if (null == sEmptyListControl || 0 == sEmptyListControl.length())
			return true;
		String[] sEmptyCoverflowControls = Utilities.splitCommaSeparatedString(sEmptyListControl);
		AppembleActivity appembleActivity = Utilities.getActivity(getContext(), this, null);
		Vector<ControlModel> vControls = appembleActivity.getAllControls();
		for (ControlModel controlModelForEmptyList : vControls) {
			if (Utilities.contains(sEmptyCoverflowControls, controlModelForEmptyList.sName)) {
				View v = AppembleActivityImpl.findViewInScreen(appembleActivity, controlModelForEmptyList);
				if (null != v)
					v.setVisibility(bVisibility ? View.VISIBLE : View.INVISIBLE);
				}
		}
		return true;
	}

    /**
     * Get the Centre of the Coverflow.
     * 
     * @return The centre of this Coverflow.
     */
    private int getCenterOfCoverflow() {
        return (getWidth() - getPaddingLeft() - getPaddingRight()) / 2 + getPaddingLeft();
    }

    /**
     * Get the Centre of the View.
     * 
     * @return The centre of the given view.
     */
    private static int getCenterOfView(final View view) {
        return view.getLeft() + view.getWidth() / 2;
    }

    /**
     * {@inheritDoc}
     * 
     * @see #setStaticTransformationsEnabled(boolean)
     */
    @Override
    protected boolean getChildStaticTransformation(final View child, final Transformation t) {

        final int childCenter = getCenterOfView(child);
        final int childWidth = child.getWidth();
        int rotationAngle = 0;

        t.clear();
        t.setTransformationType(Transformation.TYPE_MATRIX);

        if (childCenter == mCoveflowCenter) {
            transformImageBitmap((ImageView) child, t, 0);
        } else {
            rotationAngle = (int) ((float) (mCoveflowCenter - childCenter) / childWidth * mMaxRotationAngle);
            if (Math.abs(rotationAngle) > mMaxRotationAngle) {
                rotationAngle = rotationAngle < 0 ? -mMaxRotationAngle : mMaxRotationAngle;
            }
            transformImageBitmap((ImageView) child, t, rotationAngle);
        }

        return true;
    }

    /**
     * This is called during layout when the size of this view has changed. If
     * you were just added to the view hierarchy, you're called with the old
     * values of 0.
     * 
     * @param w
     *            Current width of this view.
     * @param h
     *            Current height of this view.
     * @param oldw
     *            Old width of this view.
     * @param oldh
     *            Old height of this view.
     */
    @Override
    protected void onSizeChanged(final int w, final int h, final int oldw, final int oldh) {
        mCoveflowCenter = getCenterOfCoverflow();
        super.onSizeChanged(w, h, oldw, oldh);
    }

    /**
     * Transform the Image Bitmap by the Angle passed.
     * 
     * @param imageView
     *            ImageView the ImageView whose bitmap we want to rotate
     * @param t
     *            transformation
     * @param rotationAngle
     *            the Angle by which to rotate the Bitmap
     */
    private void transformImageBitmap(final ImageView child, final Transformation t, final int rotationAngle) {
        mCamera.save();
        final Matrix imageMatrix = t.getMatrix();

        final int height = child.getLayoutParams().height;

        final int width = child.getLayoutParams().width;
        final int rotation = Math.abs(rotationAngle);

        mCamera.translate(0.0f, 0.0f, 100.0f);

        // As the angle of the view gets less, zoom in
        if (rotation < mMaxRotationAngle) {
            final float zoomAmount = (float) (mMaxZoom + rotation * 1.5);
            mCamera.translate(0.0f, 0.0f, zoomAmount);
        }

        mCamera.rotateY(rotationAngle);
        mCamera.getMatrix(imageMatrix);
        imageMatrix.preTranslate(-(width / 2.0f), -(height / 2.0f));
        imageMatrix.postTranslate((width / 2.0f), (height / 2.0f));
        mCamera.restore();
    }

    /**
     * Parses the attributes.
     * 
     * @param context
     *            the context
     * @param attrs
     *            the attrs
     */
    private void parseAttributes() {
          float iSpacing = Utilities.getDimension(controlModel.mExtendedProperties.get("spacing"), fWidth);
          setSpacing((int)iSpacing);
    }
}