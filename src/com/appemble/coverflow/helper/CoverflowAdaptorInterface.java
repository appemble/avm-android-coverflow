package com.appemble.coverflow.helper;

public interface CoverflowAdaptorInterface {
	public int getCount();
	public boolean hasData();
	public boolean moveToPosition(int iPosition);
	public boolean moveToNext();
	public Object getValue(int iDataType);
	public Object getItem(int iPosition);
	public void changeDataset(Object object);
}
