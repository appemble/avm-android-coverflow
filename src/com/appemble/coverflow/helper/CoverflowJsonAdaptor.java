/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.coverflow.helper;

import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.appemble.avm.LogManager;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class CoverflowJsonAdaptor extends BaseAdapter implements CoverflowAdaptorInterface {
    CoverflowAdaptor avmAdapter;
    JSONArray jsonArray; int iCurrentPosition = -1;
	public CoverflowJsonAdaptor(Context ctx, View listView, JSONArray jsonA, ControlModel controlModel, 
	    Vector<ControlModel> vCoverflowControls, float fParentWidth, float fParentHeight, 
	    ScreenModel screenModel) {
		super();
		jsonArray = jsonA;
		avmAdapter = new CoverflowAdaptor(ctx, listView, controlModel, vCoverflowControls,
		    screenModel, fParentWidth, fParentHeight, this);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return avmAdapter.getView(position, convertView, parent);
	}	

	@Override
	public int getCount() {
		return jsonArray != null ? jsonArray.length() : 0;
	}

	@Override
	public Object getItem(int position) {
		if (jsonArray != null && position < jsonArray.length()) {
			try {
				return jsonArray.get(position);
			} catch (JSONException e) {
				LogManager.logWTF(e, "Unable to get jsonObject at position: " + position + " for control: " + 
						avmAdapter.coverflowModel.sName);
			}
		}
		return null;
	}

	public void changeDataset(Object object) {
		try {
			jsonArray = (JSONArray) object;
		} catch (ClassCastException cce) {
			LogManager.logError("Cannot change dataset for control: " + 
			    avmAdapter.coverflowModel.sName + ". Must be a JSONArray");
		}
		moveToPosition(-1);
	}
	
	public boolean hasData() { 
		return null != jsonArray && jsonArray.length() > 0; 
	}
	
//	public int getColumnCount() {
//		if (null == jsonArray || jsonArray.length() == 0)
//			return 0;
//		try {
//			return jsonArray.getJSONObject(0).length();
//		} catch (JSONException e) {
//			LogManager.logWTF(e, "Unable to get column count for Json object of control: " + 
//			    avmAdapter.controlModel.sName);
//		}
//		return 0;
//	}
	
//	public int getColumnIndex(String sColumnName) {
//		return null != jsonArray ? jsonArray.getJSONObject(iCurrentPosition).get(sColumnName) : -1;
//	}
//	
	public boolean moveToPosition(int iPosition) {
		if (iPosition == -1)
			iCurrentPosition = iPosition;
		if (jsonArray != null && iCurrentPosition < jsonArray.length()) {
			iCurrentPosition = iPosition;
			return true;
		}
		return false;
	}
	
	public boolean moveToNext() {
		return moveToPosition(iCurrentPosition+1);
	}

	public Object getValue(int iDataType) {
		if (iCurrentPosition < 0 || jsonArray == null || iCurrentPosition > jsonArray.length())
			return null;
		try {
			return jsonArray.getString(iCurrentPosition);
		} catch (JSONException e) {
			LogManager.logWTF(e, "Unable to get value for postion: " + 
					iCurrentPosition + " of control: " + avmAdapter.coverflowModel.sName);
			return null;
		}
	}

	public long getItemId(int position) {
	    return position;
//		if (position < 0 || jsonArray == null || position > jsonArray.length())
//			return 0;
//		try {
//			String sId = jsonArray.getJSONObject(position).getString("_id");
//			if (null == sId || sId.length() == 0)
//				return 0;
//			try {
//				return Long.parseLong(sId);
//			} catch (NumberFormatException nme) {
//				return sId.hashCode();				
//			}
//		} catch (JSONException e) {
//			LogManager.logWTF(e, "Unable to get ItemId for position: " + 
//					iCurrentPosition + " of control: " + avmAdapter.controlModel.sName);
//			return 0;
//		}
	}
}