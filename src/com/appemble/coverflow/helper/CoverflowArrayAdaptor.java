/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.coverflow.helper;

import java.util.ArrayList;
import java.util.Vector;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class CoverflowArrayAdaptor extends ArrayAdapter<String> 
implements CoverflowAdaptorInterface {
    CoverflowAdaptor avmAdapter;
    ArrayList<String> aValues; int iCurrentPosition = -1;

    public CoverflowArrayAdaptor(Context ctx, View coverflowView, ArrayList<String> aVals, 
    		ControlModel controlModel, Vector<ControlModel> vCoverflowControls, float fParentWidth,
    		float fParentHeight, ScreenModel screenModel) {
		super(ctx, 0);
		aValues = aVals;
		avmAdapter = new CoverflowAdaptor(ctx, coverflowView, controlModel, vCoverflowControls, 
		    screenModel, fParentWidth, fParentHeight, this);
	}
	
	// Article reference: http://stackoverflow.com/questions/5183813/android-issue-with-newview-and-bindview-in-custom-simplecursoradapter 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return avmAdapter.getView(position, convertView, parent);
	}	

	@Override
	public int getCount() {
		return aValues.size();
	}

	@Override
	public String getItem(int position) {
		if (position < aValues.size()) 
			return aValues.get(position);
		return null;
	}

	@SuppressWarnings("unchecked")
	public void changeDataset(Object object) {
		if (!(object instanceof ArrayList))
			return;			
		aValues = (ArrayList<String>) object;
	}
	
	public boolean hasData() { 
		return null != aValues && aValues.size() > 0; 
	}
	
	public int getColumnCount() {
	    return 1;
	}
	
	public boolean moveToPosition(int iPosition) {
		if (iPosition < aValues.size()) {
			iCurrentPosition = iPosition;
			return true;
		}
		return false;
	}
	
	public boolean moveToNext() {
		return moveToPosition(iCurrentPosition+1);
	}

	public Object getValue(String sColumnName, int iDataType) {
		if (aValues == null || aValues.size() < iCurrentPosition)
			return null;
		return aValues.get(iCurrentPosition);
	}

    @Override
    public Object getValue(int iDataType) {
        return null;
    }
}