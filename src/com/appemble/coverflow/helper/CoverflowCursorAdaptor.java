/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.coverflow.helper;

import java.util.Vector;

import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

import com.appemble.avm.Utilities;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class CoverflowCursorAdaptor extends CursorAdapter implements CoverflowAdaptorInterface  {
	CoverflowAdaptor avmCoverflowAdaptor;
    
	public CoverflowCursorAdaptor(Context ctx, View coverflowView, Cursor c, ControlModel controlModel, 
	    Vector<ControlModel> vCoverflowControls, float fParentWidth, float fParentHeight, 
	    ScreenModel screenModel) {
		super(ctx, c);
		avmCoverflowAdaptor = new CoverflowAdaptor(ctx, coverflowView, controlModel, vCoverflowControls,
		    screenModel, fParentWidth, fParentHeight, this);
		if (c != null)
			c.registerDataSetObserver(mDataSetObserver);				
	}

	private DataSetObserver mDataSetObserver = new DataSetObserver() {
		public void onChanged() {
		};

		public void onInvalidated() {
		};
	}; 

	// Article reference: http://stackoverflow.com/questions/5183813/android-issue-with-newview-and-bindview-in-custom-simplecursoradapter 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return avmCoverflowAdaptor.getView(position, convertView, parent);
	}	

	@Override
	public int getCount() {
		return super.getCount();
	}

	@Override
	public void changeCursor(Cursor cursor) {
		Cursor oldCursor = getCursor(); 
		if(oldCursor != null && false == oldCursor.isClosed()) {
			oldCursor.unregisterDataSetObserver(mDataSetObserver);
		}

		super.changeCursor(cursor);
		if (cursor != null && false == cursor.isClosed())
			cursor.registerDataSetObserver(mDataSetObserver);
		if(oldCursor != null && false == oldCursor.isClosed()) {
			oldCursor.close();
		}
	}
	
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		return null;
	}

	@Override
	public boolean hasData() {
		Cursor cursor = getCursor();
		return null != cursor && cursor.getCount() > 0; 
	}

	public int getColumnCount() {
		Cursor cursor = getCursor();
		return null != cursor ? cursor.getColumnCount() : 0;
	}

	@Override
	public boolean moveToPosition(int iPosition) {
		Cursor cursor = getCursor();
		return null != cursor ? cursor.moveToPosition(iPosition) : false;
	}

	@Override
	public boolean moveToNext() {
		Cursor cursor = getCursor();
		return null != cursor ? cursor.moveToNext() : false;
	}

	public int getColumnIndex(String sColumnName) {
		Cursor cursor = getCursor();
		return null != cursor ? cursor.getColumnIndex(sColumnName) : -1;
	}
	
	public Object getValue(String sColumnName, int iDataType) {
		if (null == sColumnName)
			return null;
		int iColumnIndex = getColumnIndex(sColumnName);
		Cursor cursor = getCursor();
		return iColumnIndex >= 0 && iColumnIndex < cursor.getColumnCount() ? 
				Utilities.getCursorData(cursor, iColumnIndex, iDataType) : null;
	}

	@Override
	public void changeDataset(Object object) {
		Cursor c = (Cursor)object;
		changeCursor(c);
	}

    @Override
    public Object getValue(int iDataType) {
        Cursor cursor = getCursor();
        return null != cursor ? Utilities.getCursorData(cursor, 0, iDataType) : -1;
    }	
}