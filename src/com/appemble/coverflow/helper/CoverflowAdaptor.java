/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.coverflow.helper;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;

import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.controls.IMAGE;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.coverflow.controls.COVERFLOW;

public class CoverflowAdaptor {
    ControlModel coverflowModel;
    ControlModel imageControlModel;
    Context context;
    COVERFLOW coverflowView;
	ScreenModel screenModel;
	float fWidth, fHeight; // Using which the child control dimensions are to be calculated.
    CoverflowAdaptorInterface dataSource = null;

    /** The bitmap map. */
    private final Map<Integer, WeakReference<Bitmap>> bitmapMap = new HashMap<Integer, WeakReference<Bitmap>>();

	public CoverflowAdaptor(Context ctx, View coverflowView, ControlModel controlModel, 
        Vector<ControlModel> vCoverflowControls, ScreenModel screenModel, float fParentWidth, float fParentHeight,
		CoverflowAdaptorInterface dataSource) {
		context = ctx;
		if (coverflowView instanceof COVERFLOW)
		    this.coverflowView = (COVERFLOW)coverflowView;
		this.coverflowModel = controlModel;
		this.screenModel = screenModel;
		this.fWidth = fParentWidth;
		this.fHeight = fParentHeight;
		this.dataSource = dataSource;
		if (vCoverflowControls != null && vCoverflowControls.size() > 0)
            imageControlModel = vCoverflowControls.get(0);
	}

	// Article reference: http://stackoverflow.com/questions/5183813/android-issue-with-newview-and-bindview-in-custom-simplecursoradapter 
	public synchronized View getView(final int position, final View convertView, ViewGroup parent) {
	    if (coverflowModel == null || imageControlModel == null)
	        return null; // these params are needed for this function.
		IMAGE imageView = null;
        if (convertView == null) {
            final Context context = parent.getContext();
            imageView = new IMAGE(context, imageControlModel);
            imageView.initialize(coverflowView, fWidth, fHeight, (Vector<ControlModel>)null, screenModel);
            imageView.setLayoutParams(new COVERFLOW.LayoutParams(
                (int) Utilities.getDimension(imageControlModel.sWidth, fWidth), 
                (int) Utilities.getDimension(imageControlModel.sHeight, fHeight)));
        } else {
            if ((false == (convertView instanceof IMAGE)))
                return null; // convertView must be of type IMAGE
            imageView = (IMAGE)convertView;
        }
        final WeakReference<Bitmap> weakBitmapReference = bitmapMap.get(position);
        if (weakBitmapReference != null) {
            final Bitmap bitmap = weakBitmapReference.get();
            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
                return imageView;
            }
        }
        if (false == dataSource.hasData() || false == dataSource.moveToPosition(position)) 
            return imageView;
        Object imageSource = dataSource.getValue(position);
        if (imageSource instanceof String) {
            imageView.setValue(imageSource);
        }
        return imageView;
	}

	private void onCreateCoverflowImage(View convertView) {
	    Action.callAction(context, convertView, coverflowView, coverflowModel, 
	        COVERFLOW.ON_CREATE_COVERFLOW_IMAGE);
    }
}